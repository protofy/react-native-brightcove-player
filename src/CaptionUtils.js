const key = 'react-native-brightcove-player/preferred_caption_languages';

/**
 * Fetches the preferred subtitle language list from the persistence layer.
 * @param {PersistenceRead} read
 * @param {PersistenceWrite} write
 * @returns {Promise<string[]>}
 */
const getPreferredSubtitleLanguages = (read, write) => {
  return read(key).then((preferredSubtitles) => {
    if (preferredSubtitles === null) {
      return write(key, JSON.stringify([])).then(() => []);
    }

    return JSON.parse(preferredSubtitles);
  });
};

/**
 * Adds a language to the preferred subtitle languages. Removed if pre-existing.
 * The given language will have the highest priority.
 *
 * @param {PersistenceRead} read
 * @param {PersistenceWrite} write
 * @param {string} language
 * @returns {Promise<void>}
 */
const addPreferredSubtitleLanguage = (read, write, language) => {
  return getPreferredSubtitleLanguages(read, write).then((preferredSubtitles) => write(key, JSON.stringify([language, ...preferredSubtitles.filter((preferredLanguage => preferredLanguage !== language))])));
};

/**
 * Return current active caption, if there is any.
 *
 * @param {PersistenceRead} read
 * @param {PersistenceWrite} write
 * @param {Caption[]} captions
 * @returns {Promise<Caption|undefined>}
 */
const getActiveCaption = (read, write, captions = []) => getPreferredSubtitleLanguages(read, write).then((preferredLanguages) => {
  const captionLanguages = captions.map((caption) => caption.language);
  const captionLanguage = preferredLanguages.find((language) => captionLanguages.includes(language));

  if (captionLanguage === undefined) {
    return undefined;
  }

  return captions.find((caption) => caption.language === captionLanguage);
});

/**
 * Removes the caption configuration from the persistence layer.
 *
 * @param {PersistenceDelete} remove
 * @returns {Promise<void>}
 */
const resetCaptionSelection = (remove) => remove(key);

/**
 *
 * @param {PersistenceRead} read
 * @param {PersistenceWrite} write
 * @param {PersistenceDelete} remove
 * @returns {{getPreferredSubtitleLanguages: (function(): Promise<string[]>), addPreferredSubtitleLanguage: (function(language: string): Promise<void>), getActiveCaption: (function(captions: Caption[]): Promise<string[]>), resetCaptionSelection: (function(): Promise<void>)}}
 * @constructor
 */
export const getCaptionService = (read, write, remove) => ({  getPreferredSubtitleLanguages: () => getPreferredSubtitleLanguages(read, write),
  addPreferredSubtitleLanguage: (language) => addPreferredSubtitleLanguage(read, write, language),
  getActiveCaption: (captions) => getActiveCaption(read, write, captions),
  resetCaptionSelection: () => resetCaptionSelection(remove),
});
