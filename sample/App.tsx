import React, {useCallback, useEffect, useRef, useState} from 'react';
import {
  FlatList,
  Modal,
  Platform,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {
  BrightcovePlayer,
  BrightcovePlayerPoster,
  BrightcovePlayerUtil,
  Caption,
} from 'react-native-brightcove-player';
import AirPlayButton from 'react-native-airplay-button';
import {captionService} from './captionService';
import Subtitles from 'react-native-subtitles';

const ACCOUNT_ID = '5434391461001';
const POLICY_KEY =
  'BCpkADawqM0T8lW3nMChuAbrcunBBHmh4YkNl5e6ZrKQwPiK_Y83RAOF4DP5tyBF_ONBVgrEjqW6fbV0nKRuHvjRU3E8jdT9WMTOXfJODoPML6NUDCYTwTHxtNlr5YdyGYaCPLhMUZ3Xu61L';
const PLAYLIST_REF_ID = 'brightcove-native-sdk-plist';

export default function App() {
  const playerRef = useRef(null);
  const [playing, setPlaying] = useState<boolean | null>(null);
  const [showCustomControls, setShowCustomControls] = useState(false);
  const [allowExternalPlayback, setAllowExternalPlayback] = useState(false);
  const [videos, setVideos] = useState<BrightcovePlayerUtil.PlaylistVideo[]>(
    [],
  );
  const [captions, setCaptions] = useState<Caption[]>([]);
  const [activeCaption, setActiveCaption] = useState<Caption | undefined>(
    undefined,
  );
  const [offlineVideos, setOfflineVideos] = useState<
    BrightcovePlayerUtil.OfflineVideoStatus[]
  >([]);
  const [playback, setPlayback] = useState<{
    referenceId?: string;
    videoToken?: string;
  }>({
    referenceId: undefined,
    videoToken: undefined,
  });
  const [currentTime, setCurrentTime] = useState(0);
  const [showModal, setShowModal] = useState(false);

  useEffect(() => {
    BrightcovePlayerUtil.getPlaylistWithReferenceId(
      ACCOUNT_ID,
      POLICY_KEY,
      PLAYLIST_REF_ID,
    )
      .then(setVideos)
      .catch(console.warn);

    BrightcovePlayerUtil.getOfflineVideoStatuses(ACCOUNT_ID, POLICY_KEY)
      .then(setOfflineVideos)
      .catch(console.warn);

    const disposer =
      BrightcovePlayerUtil.addOfflineNotificationListener(setOfflineVideos);

    return disposer?.remove();
  }, []);

  useEffect(() => {
    captionService.getActiveCaption(captions).then(setActiveCaption);
  }, [captions, showModal]);

  const requestDownload = (videoId: any) => {
    BrightcovePlayerUtil.requestDownloadVideoWithVideoId(
      ACCOUNT_ID,
      POLICY_KEY,
      videoId,
    ).catch(() => {});
  };

  const playVideo = useCallback(
    (item: any) => {
      const downloadStatus = offlineVideos.find(
        video => video.videoId === item.videoId,
      );

      if (downloadStatus && downloadStatus.downloadProgress === 1) {
        setPlayback({videoToken: downloadStatus.videoToken});
      } else {
        setPlayback({referenceId: item.referenceId});
      }
    },
    [offlineVideos],
  );

  const deleteDownloadedVideo = (videoToken: any) => {
    BrightcovePlayerUtil.deleteOfflineVideo(
      ACCOUNT_ID,
      POLICY_KEY,
      videoToken,
    ).catch(console.warn);
  };

  // console.warn(activeCaption);

  return (
    <View style={styles.container}>
      <StatusBar barStyle="light-content" />
      <Modal onRequestClose={() => setShowModal(false)} visible={showModal}>
        <View style={styles.captionSelectionModal}>
          <TouchableOpacity
            onPress={() => {
              captionService
                .resetCaptionSelection()
                .then(() => setShowModal(false));
            }}>
            <Text>None</Text>
          </TouchableOpacity>
          {captions.map(caption => (
            <TouchableOpacity
              key={caption.language}
              onPress={() => {
                captionService
                  .addPreferredSubtitleLanguage(caption.language)
                  .then(() => setShowModal(false));
              }}>
              <Text>{caption.label}</Text>
            </TouchableOpacity>
          ))}
        </View>
      </Modal>
      <BrightcovePlayer
        ref={playerRef}
        style={styles.video}
        accountId={ACCOUNT_ID}
        policyKey={POLICY_KEY}
        autoPlay={playing == null}
        onProgress={({currentTime: progress}) => setCurrentTime(progress)}
        allowExternalPlayback={allowExternalPlayback}
        disableDefaultControl={showCustomControls}
        onClosedCaptionsLoaded={event => {
          setCaptions(event.captions);
        }}
        onPlay={() => setPlaying(true)}
        onPause={() => setPlaying(false)}
        {...(playing === null ? {} : {play: playing})}
        {...playback}
      />
      {activeCaption && (
        <View style={styles.captionContainer} pointerEvents={'none'}>
          <Subtitles
            selectedsubtitle={{
              file: activeCaption.uri.replace('http', 'https'),
            }}
            currentTime={currentTime}
            textStyle={styles.captionText}
          />
        </View>
      )}
      <FlatList
        style={styles.list}
        extraData={offlineVideos}
        data={videos}
        keyExtractor={item => item.referenceId}
        renderItem={({item}) => {
          const downloadStatus = offlineVideos.find(
            video => video.videoId === item.videoId,
          );
          return (
            <View style={styles.listItem}>
              <TouchableOpacity
                style={styles.mainButton}
                onPress={() => playVideo(item)}>
                <BrightcovePlayerPoster
                  style={styles.poster}
                  accountId={ACCOUNT_ID}
                  policyKey={POLICY_KEY}
                  referenceId={item.referenceId}
                />
                <View style={styles.body}>
                  <Text style={styles.name}>{item.name}</Text>
                  <Text>{item.description}</Text>
                  {downloadStatus ? (
                    <Text style={styles.offlineBanner}>
                      {downloadStatus.downloadProgress === 1
                        ? 'OFFLINE PLAYBACK'
                        : `DOWNLOADING: ${Math.floor(
                            downloadStatus.downloadProgress * 100,
                          )}%`}
                    </Text>
                  ) : null}
                  <Text style={styles.duration}>
                    {`0${Math.floor(item.duration / 60000) % 60}`.substring(-2)}
                    :{`0${Math.floor(item.duration / 1000) % 60}`.substring(-2)}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.downloadButton}
                onPress={() => {
                  if (!downloadStatus) {
                    requestDownload(item.videoId);
                  } else {
                    deleteDownloadedVideo(downloadStatus.videoToken);
                  }
                }}>
                <Text>
                  {!downloadStatus
                    ? '💾'
                    : downloadStatus.downloadProgress === 1
                    ? '🗑'
                    : '⏳'}
                </Text>
              </TouchableOpacity>
            </View>
          );
        }}
      />
      {playback.referenceId === null && playback.videoToken === null ? (
        <></>
      ) : showCustomControls ? (
        <View style={styles.customControlsContainerWrapper}>
          <TouchableOpacity
            style={styles.closeControlsButton}
            onPress={() => setShowCustomControls(!showCustomControls)}>
            <Text style={styles.closeControlsButtonText}>X</Text>
          </TouchableOpacity>

          <View style={styles.customControlsContainer}>
            <View style={styles.controlsRow}>
              <View style={styles.controlsCell}>
                <TouchableOpacity
                  onPress={() => setAllowExternalPlayback(prev => !prev)}>
                  <Text style={styles.customControlText}>
                    {allowExternalPlayback ? 'Deactivate' : 'Activate'} external
                    playback
                  </Text>
                </TouchableOpacity>
              </View>

              {Platform.OS === 'ios' ? (
                <View style={styles.externalPlaybackButtonContainer}>
                  <AirPlayButton
                    activeTintColor="blue"
                    tintColor="black"
                    prioritizesVideoDevices={true}
                    style={styles.airplayButton}
                  />
                </View>
              ) : (
                <></>
              )}
            </View>

            <View style={styles.controlsRow}>
              <View style={styles.controlsCell}>
                <TouchableOpacity
                  onPress={() =>
                    setPlaying(prev => !(prev == null || prev === true))
                  }>
                  <Text style={styles.customControlText}>
                    {playing == null || playing === true ? 'Pause' : 'Play'}
                  </Text>
                </TouchableOpacity>
              </View>
              {captions.length > 0 && (
                <View style={styles.controlsCell}>
                  <TouchableOpacity onPress={() => setShowModal(true)}>
                    <Text style={styles.customControlText}>Show Captions</Text>
                  </TouchableOpacity>
                </View>
              )}
            </View>
          </View>
        </View>
      ) : (
        <View style={styles.customControlsSwitchContainer}>
          <View style={styles.customControlsContainer}>
            <View style={styles.controlsRow}>
              <TouchableOpacity
                style={styles.controlsCell}
                onPress={() => setShowCustomControls(!showCustomControls)}>
                <Text style={styles.customControlText}>
                  Enable Custom Controls
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  video: {
    width: '100%',
    height: 260,
  },
  list: {
    flex: 1,
  },
  listItem: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: 'lightgray',
  },
  mainButton: {
    flex: 1,
    flexDirection: 'row',
  },
  body: {
    flex: 1,
    padding: 10,
    flexDirection: 'column',
  },
  name: {
    fontSize: 14,
    fontWeight: 'bold',
  },
  offlineBanner: {
    fontSize: 10,
    fontWeight: 'bold',
    color: 'white',
    alignSelf: 'flex-start',
    padding: 3,
    backgroundColor: 'deepskyblue',
  },
  duration: {
    marginTop: 'auto',
    opacity: 0.5,
  },
  poster: {
    width: 100,
    height: 100,
    backgroundColor: 'black',
  },
  downloadButton: {
    padding: 16,
    marginLeft: 'auto',
    alignSelf: 'center',
  },
  controlsRow: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  controlsCell: {
    flex: 1,
    margin: 8,
    borderColor: 'rgba(0,0,0,0.75)',
    borderWidth: 2,
    borderRadius: 20,
    padding: 8,
    alignItems: 'center',
    backgroundColor: 'orange',
  },
  closeControlsButton: {
    height: 36,
    width: 36,
    borderRadius: 50,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: 'black',
    position: 'absolute',
    backgroundColor: 'lightgray',
    right: 5,
    top: -16,
    zIndex: 1,
    ...Platform.select({
      android: {
        elevation: 1,
      },
    }),
  },
  closeControlsButtonText: {fontSize: 22, color: 'red'},
  customControlText: {
    fontSize: 24,
    textAlign: 'center',
    textAlignVertical: 'center',
    alignSelf: 'center',
  },
  customControlsContainer: {
    backgroundColor: 'lightgray',
    flexDirection: 'column',
    flex: 1,
    borderTopWidth: 1,
    borderTopColor: 'rgba(0,0,0,0.25)',
    padding: 8,
  },
  customControlsSwitchContainer: {
    height: 84,
  },
  airplayButton: {width: 64, height: 64},
  externalPlaybackButtonContainer: {
    flex: 1,
    flexBasis: 64,
    flexGrow: 0,
    margin: 8,
    borderColor: 'rgba(0,0,0,0.75)',
    borderWidth: 2,
    borderRadius: 20,
    padding: 8,
    alignItems: 'center',
  },
  customControlsContainerWrapper: {height: '50%'},
  captionContainer: {
    position: 'absolute',
    top: 260 - 120,
    zIndex: 3636,
    elevation: 3636,
    left: 0,
    right: 0,
  },
  captionText: {color: 'white', fontSize: 14},
  captionSelectionModal: {flex: 1, paddingTop: 100},
});
