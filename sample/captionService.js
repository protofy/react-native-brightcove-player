import {getCaptionService} from 'react-native-brightcove-player';
import AsyncStorage from '@react-native-async-storage/async-storage';

export const captionService = getCaptionService(
  AsyncStorage.getItem,
  AsyncStorage.setItem,
  AsyncStorage.removeItem,
);
