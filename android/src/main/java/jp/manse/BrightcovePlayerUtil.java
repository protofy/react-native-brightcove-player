package jp.manse;

import android.net.Uri;
import android.util.Pair;

import androidx.annotation.NonNull;

import com.brightcove.player.captioning.BrightcoveCaptionFormat;
import com.brightcove.player.edge.Catalog;
import com.brightcove.player.edge.CatalogError;
import com.brightcove.player.edge.OfflineCallback;
import com.brightcove.player.edge.OfflineCatalog;
import com.brightcove.player.edge.VideoListener;
import com.brightcove.player.model.Video;
import com.brightcove.player.offline.MediaDownloadable;
import com.facebook.react.bridge.NativeArray;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableNativeArray;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeArray;
import com.facebook.react.bridge.WritableNativeMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import jp.manse.util.DefaultEventEmitter;

public class BrightcovePlayerUtil extends ReactContextBaseJavaModule implements BrightcovePlayerAccount.OnBrightcovePlayerAccountListener {

    final private static String CALLBACK_OFFLINE_NOTIFICATION = "OfflineNotification";
    final private static String ERROR_CODE = "error";
    final private static String ERROR_MESSAGE_MISSING_ARGUMENTS = "Both accountId and policyKey must not be null";

    private final List<BrightcovePlayerAccount> brightcovePlayerAccounts = new ArrayList<>();

    public BrightcovePlayerUtil(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @NonNull
    @Override
    public String getName() {
        return "BrightcovePlayerUtil";
    }

    // Required for rn built in EventEmitter Calls.
    @ReactMethod
    public void addListener(String eventName) {

    }

    @ReactMethod
    public void removeListeners(Integer count) {

    }

    @ReactMethod
    public void requestDownloadVideoWithReferenceId(String referenceId, String accountId, String policyKey, int bitRate, Promise promise) {
        BrightcovePlayerAccount account = this.getBrightcovePlayerAccount(accountId, policyKey);
        if (account == null) {
            promise.reject(ERROR_CODE, ERROR_MESSAGE_MISSING_ARGUMENTS);
            return;
        }
        account.requestDownloadWithReferenceId(referenceId, bitRate, promise);
    }

    @ReactMethod
    public void requestDownloadVideoWithVideoId(String videoId, String accountId, String policyKey, int bitRate, Promise promise) {
        BrightcovePlayerAccount account = this.getBrightcovePlayerAccount(accountId, policyKey);
        if (account == null) {
            promise.reject(ERROR_CODE, ERROR_MESSAGE_MISSING_ARGUMENTS);
            return;
        }
        account.requestDownloadWithVideoId(videoId, bitRate, promise);
    }

    @ReactMethod
    public void getOfflineVideoStatuses(String accountId, String policyKey, Promise promise) {
        BrightcovePlayerAccount account = this.getBrightcovePlayerAccount(accountId, policyKey);
        if (account == null) {
            promise.reject(ERROR_CODE, ERROR_MESSAGE_MISSING_ARGUMENTS);
            return;
        }
        account.getOfflineVideoStatuses(promise);
    }

    @ReactMethod
    public void deleteOfflineVideo(String accountId, String policyKey, String videoId, Promise promise) {
        BrightcovePlayerAccount account = this.getBrightcovePlayerAccount(accountId, policyKey);
        if (account == null) {
            promise.reject(ERROR_CODE, ERROR_MESSAGE_MISSING_ARGUMENTS);
            return;
        }
        account.deleteOfflineVideo(videoId, promise);
    }

    @ReactMethod
    public void getPlaylistWithPlaylistId(String playlistId, String accountId, String policyKey, Promise promise) {
        BrightcovePlayerAccount account = this.getBrightcovePlayerAccount(accountId, policyKey);
        if (account == null) {
            promise.reject(ERROR_CODE, ERROR_MESSAGE_MISSING_ARGUMENTS);
            return;
        }
        account.getPlaylistWithPlaylistId(playlistId, promise);
    }

    @ReactMethod
    public void getPlaylistWithReferenceId(String referenceId, String accountId, String policyKey, Promise promise) {
        BrightcovePlayerAccount account = this.getBrightcovePlayerAccount(accountId, policyKey);
        if (account == null) {
            promise.reject(ERROR_CODE, ERROR_MESSAGE_MISSING_ARGUMENTS);
            return;
        }
        account.getPlaylistWithReferenceId(referenceId, promise);
    }

    @Override
    public void onOfflineStorageStateChanged(NativeArray array) {
        this.sendOfflineNotification(array);
    }

    private void sendOfflineNotification(NativeArray array) {
        this.getReactApplicationContext().getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit(CALLBACK_OFFLINE_NOTIFICATION, array);
    }

    private BrightcovePlayerAccount getBrightcovePlayerAccount(String accountId, String policyKey) {
        if (accountId == null || policyKey == null) return null;
        for (BrightcovePlayerAccount owner : this.brightcovePlayerAccounts) {
            if (owner.accountId.equals(accountId) && policyKey.equals(policyKey)) {
                return owner;
            }
        }
        BrightcovePlayerAccount owner = new BrightcovePlayerAccount(this.getReactApplicationContext(), accountId, policyKey, this);
        this.brightcovePlayerAccounts.add(owner);
        return owner;
    }


    /**
     * Determines the estimated size including all assets(thumbnail, sound, subtitles) of a downloaded video
     */
    @ReactMethod
    public void getEstimatedSizeOfDownloadedVideo(final String videoId, final String accountId, final String policyKey, final Promise estimateDownloadedVideoSizePromise) {

        assert estimateDownloadedVideoSizePromise != null;

        if (accountId == null || accountId.isEmpty()) {
            estimateDownloadedVideoSizePromise.reject(new IllegalArgumentException("Parameter accountId must not be null or empty"));
            return;
        }

        if (policyKey == null || policyKey.isEmpty()) {
            estimateDownloadedVideoSizePromise.reject(new IllegalArgumentException("Parameter policyKey must not be null or empty"));
            return;
        }

        if (videoId == null || videoId.isEmpty()) {
            estimateDownloadedVideoSizePromise.reject(new IllegalArgumentException("Parameter videoId must not be null or empty"));
            return;
        }

        final OfflineCatalog offlineCatalog = new OfflineCatalog(getReactApplicationContext(), DefaultEventEmitter.sharedEventEmitter, accountId, policyKey);

        final MediaDownloadable.OnVideoSizeCallback onVideoSizeCallback = new MediaDownloadable.OnVideoSizeCallback() {
            @Override
            public void onVideoSizeEstimated(long estimatedVideoSizeBytes) {
                estimateDownloadedVideoSizePromise.resolve(estimatedVideoSizeBytes * 0.000001D);
            }
        };

        final OfflineCallback<Video> findOfflineVideoByIdCallback = new OfflineCallback<Video>() {

            @Override
            public void onSuccess(Video video) {
                if (video != null) {
                    offlineCatalog.estimateSize(video, onVideoSizeCallback);
                } else {
                    estimateDownloadedVideoSizePromise.reject(new IllegalArgumentException("Could not find downloaded video for id " + videoId));
                }
            }

            @Override
            public void onFailure(Throwable throwable) {
                estimateDownloadedVideoSizePromise.reject(throwable);
            }
        };

        offlineCatalog.findOfflineVideoById(videoId, findOfflineVideoByIdCallback);
    }
}
