package jp.manse;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.NonNull;

import com.brightcove.player.edge.Catalog;
import com.brightcove.player.edge.CatalogError;
import com.brightcove.player.edge.OfflineCallback;
import com.brightcove.player.edge.OfflineCatalog;
import com.brightcove.player.edge.PlaylistListener;
import com.brightcove.player.model.Playlist;
import com.brightcove.player.model.Video;
import com.brightcove.player.network.DownloadStatus;
import com.facebook.react.bridge.NativeArray;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.WritableNativeArray;
import com.facebook.react.bridge.WritableNativeMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Predicate;

import jp.manse.util.DefaultEventEmitter;

public class BrightcovePlayerAccount implements OfflineVideoDownloadSession.OnOfflineVideoDownloadSessionListener {

    final private static int FPS = 40;
    final private static String DEBUG_TAG = "brightcoveplayer";
    final private static String ERROR_CODE = "error";
    final private static String ERROR_MESSAGE_DUPLICATE_SESSION = "Offline video or download session already exists";
    final private static String ERROR_MESSAGE_DELETE = "Could not delete video";
    final private static String ERROR_MESSAGE_PLAYLIST = "Failed to load playlist";
    final private static String CALLBACK_KEY_VIDEO_TOKEN = "videoToken";
    final private static String CALLBACK_KEY_DOWNLOAD_PROGRESS = "downloadProgress";
    final private static String CALLBACK_KEY_VIDEO_SIZE_IN_MB = "videoSizeInMegabyte";
    final private static String CALLBACK_KEY_ACCOUNT_ID = "accountId";
    final private static String CALLBACK_KEY_VIDEO_ID = "videoId";
    final private static String CALLBACK_KEY_REFERENCE_ID = "referenceId";
    final private static String CALLBACK_KEY_NAME = "name";
    final private static String CALLBACK_KEY_DESCRIPTION = "description";
    final private static String CALLBACK_KEY_DURATION = "duration";

    private final ReactApplicationContext context;

    public String accountId;
    public String policyKey;

    private final Handler handler;
    private final List<OfflineVideoDownloadSession> offlineVideoDownloadSessions = new ArrayList<>();
    private boolean getOfflineVideoStatusesRunning = false;
    private final List<Promise> getOfflineVideoStatusesPendingPromises = new ArrayList<>();
    private final HashMap<String, Video> allDownloadedVideos = new HashMap<>();
    private final Catalog catalog;
    private final OfflineCatalog offlineCatalog;
    private final OnBrightcovePlayerAccountListener listener;

    public interface OnBrightcovePlayerAccountListener {
        void onOfflineStorageStateChanged(NativeArray array);
    }

    public BrightcovePlayerAccount(final ReactApplicationContext context, final String accountId, final String policyKey, OnBrightcovePlayerAccountListener listener) {
        this.context = context;
        this.accountId = accountId;
        this.policyKey = policyKey;
        this.listener = listener;
        handler = new Handler(Looper.myLooper());
        this.catalog = new Catalog.Builder(DefaultEventEmitter.sharedEventEmitter, accountId).setPolicy(policyKey).build();
        this.offlineCatalog = new OfflineCatalog.Builder(context, DefaultEventEmitter.sharedEventEmitter, accountId).setPolicy(policyKey).build();
        this.offlineCatalog.setMeteredDownloadAllowed(true);
        this.offlineCatalog.setMobileDownloadAllowed(true);
        this.offlineCatalog.setRoamingDownloadAllowed(true);


      this.offlineCatalog.findAllVideoDownload(DownloadStatus.STATUS_DOWNLOADING, new OfflineCallback<List<Video>>() {
        @Override
        public void onSuccess(List<Video> videos) {
          for (int i = 0; i < videos.size(); i++) {
            Video video = videos.get(i);
            OfflineVideoDownloadSession session = new OfflineVideoDownloadSession(context, accountId, policyKey, BrightcovePlayerAccount.this);
            session.resumeDownload(video);
            offlineVideoDownloadSessions.add(session);
          }
        }

        @Override
        public void onFailure(Throwable throwable) {
        }
      });

      this.offlineCatalog.findAllVideoDownload(DownloadStatus.STATUS_PENDING, new OfflineCallback<List<Video>>() {
        @Override
        public void onSuccess(List<Video> videos) {
          for (int i = videos.size() - 1; i >= 0; i--) {
            Video video = videos.get(i);
            offlineCatalog.cancelVideoDownload(video);
          }
        }

        @Override
        public void onFailure(Throwable throwable) {
        }
      });
      this.offlineCatalog.findAllVideoDownload(DownloadStatus.STATUS_QUEUEING, new OfflineCallback<List<Video>>() {
        @Override
        public void onSuccess(List<Video> videos) {
          for (int i = videos.size() - 1; i >= 0; i--) {
            Video video = videos.get(i);
            offlineCatalog.cancelVideoDownload(video);
          }
        }

        @Override
        public void onFailure(Throwable throwable) {
        }
      });
    }

    public void requestDownloadWithReferenceId(String referenceId, int bitRate, Promise promise) {
        if (this.hasOfflineVideoDownloadSessionWithReferenceId(referenceId)) {
            promise.reject(ERROR_CODE, ERROR_MESSAGE_DUPLICATE_SESSION);
            return;
        }
        OfflineVideoDownloadSession session = new OfflineVideoDownloadSession(this.context, this.accountId, this.policyKey, this);
        session.requestDownloadWithReferenceId(referenceId, bitRate, promise);
        this.offlineVideoDownloadSessions.add(session);
        this.listener.onOfflineStorageStateChanged(collectNativeOfflineVideoStatuses());
    }

    public void requestDownloadWithVideoId(String videoId, int bitRate, Promise promise) {
        if (this.hasOfflineVideoDownloadSessionWithVideoId(videoId)) {
            promise.reject(ERROR_CODE, ERROR_MESSAGE_DUPLICATE_SESSION);
            return;
        }
        OfflineVideoDownloadSession session = new OfflineVideoDownloadSession(this.context, this.accountId, this.policyKey, this);
        session.requestDownloadWithVideoId(videoId, bitRate, promise);
        this.offlineVideoDownloadSessions.add(session);
        this.listener.onOfflineStorageStateChanged(collectNativeOfflineVideoStatuses());
    }

    public void getOfflineVideoStatuses(Promise promise) {
        if (this.getOfflineVideoStatusesRunning) {
            this.getOfflineVideoStatusesPendingPromises.add(promise);
            return;
        }
        this.getOfflineVideoStatusesRunning = true;
        this.getOfflineVideoStatusesPendingPromises.clear();
        this.getOfflineVideoStatusesPendingPromises.add(promise);
        final Runnable updateRunnable = new Runnable() {
            @Override
            public void run() {
                sendOfflineVideoStatuses();
                getOfflineVideoStatusesRunning = false;
            }
        };
        this.offlineCatalog.findAllVideoDownload(DownloadStatus.STATUS_COMPLETE, new OfflineCallback<List<Video>>() {
            @Override
            public void onSuccess(List<Video> videos) {
                videos.stream().filter(new Predicate<Video>() {
                    @Override
                    public boolean test(Video video) {
                        return !allDownloadedVideos.containsKey(video.getId());
                    }
                }).forEach(new Consumer<Video>() {
                    @Override
                    public void accept(Video video) {
                        allDownloadedVideos.put(video.getId(), video);
                        offlineCatalog.getVideoDownloadStatus(video);
                    }
                });

                handler.postDelayed(updateRunnable, FPS);
            }

            @Override
            public void onFailure(Throwable throwable) {
                for (int i = 0; i < getOfflineVideoStatusesPendingPromises.size(); i++) {
                    Promise promise = getOfflineVideoStatusesPendingPromises.get(i);
                    promise.reject(ERROR_CODE, ERROR_CODE);
                }
                getOfflineVideoStatusesRunning = false;
            }
        });
    }

    private void sendOfflineVideoStatuses() {
        for (int i = 0; i < getOfflineVideoStatusesPendingPromises.size(); i++) {
            Promise promise = getOfflineVideoStatusesPendingPromises.get(i);
            promise.resolve(this.collectNativeOfflineVideoStatuses());
        }
    }

    public void deleteOfflineVideo(final String videoId, final Promise promise) {
        try {
            if (videoId == null) throw new Exception();
            this.offlineCatalog.cancelVideoDownload(videoId);
            for (int i = this.offlineVideoDownloadSessions.size() - 1; i >= 0; i--) {
                OfflineVideoDownloadSession session = this.offlineVideoDownloadSessions.get(i);
                if (videoId.equals(session.videoId)) {
                    this.offlineVideoDownloadSessions.remove(i);
                }
            }
            this.offlineCatalog.deleteVideo(videoId, new OfflineCallback<Boolean>() {
                @Override
                public void onSuccess(Boolean aBoolean) {
                    promise.resolve(null);
                    allDownloadedVideos.remove(videoId);
                    listener.onOfflineStorageStateChanged(collectNativeOfflineVideoStatuses());
                }

                @Override
                public void onFailure(Throwable throwable) {
                    promise.reject(ERROR_CODE, ERROR_MESSAGE_DELETE);
                }
            });
        } catch (Exception e) {
            Log.e(DEBUG_TAG, e.getMessage());
            promise.reject(ERROR_CODE, e);
        }
    }

    public void getPlaylistWithPlaylistId(String playlistId, final Promise promise) {
        catalog.findPlaylistByID(playlistId, new PlaylistListener() {
            @Override
            public void onPlaylist(Playlist playlist) {
                promise.resolve(collectNativePlaylist(accountId, playlist));
            }

            @Override
            public void onError(@NonNull List<CatalogError> errors) {
                promise.reject(ERROR_CODE, ERROR_MESSAGE_PLAYLIST);
            }
        });
    }

    public void getPlaylistWithReferenceId(String referenceId, final Promise promise) {
        catalog.findPlaylistByReferenceID(referenceId, new PlaylistListener() {
            @Override
            public void onPlaylist(Playlist playlist) {
                promise.resolve(collectNativePlaylist(accountId, playlist));
            }

            @Override
            public void onError(@NonNull List<CatalogError> errors) {
                promise.reject(ERROR_CODE, ERROR_MESSAGE_PLAYLIST);
            }
        });
    }

    private NativeArray collectNativeOfflineVideoStatuses() {
        final WritableNativeArray statuses = new WritableNativeArray();

        allDownloadedVideos.values().forEach(
                new Consumer<Video>() {
                    @Override
                    public void accept(Video video) {
                        long estimateSizeInMb = offlineCatalog.estimateSize(video) / 1024 / 1024;

                        WritableNativeMap map = new WritableNativeMap();
                        map.putString(CALLBACK_KEY_ACCOUNT_ID, accountId);
                        map.putString(CALLBACK_KEY_VIDEO_ID, video.getId());
                        map.putString(CALLBACK_KEY_VIDEO_TOKEN, video.getId());
                        map.putInt(CALLBACK_KEY_VIDEO_SIZE_IN_MB, (int) estimateSizeInMb);
                        map.putDouble(CALLBACK_KEY_DOWNLOAD_PROGRESS, 1);

                        statuses.pushMap(map);
                    }
                }
        );

        for (int i = 0; i < offlineVideoDownloadSessions.size(); i++) {
            OfflineVideoDownloadSession session = offlineVideoDownloadSessions.get(i);
            if (session.videoId == null) continue;

            Video video = allDownloadedVideos.get(session.videoId);
            if (video != null) continue;

            WritableNativeMap map = new WritableNativeMap();
            map.putString(CALLBACK_KEY_ACCOUNT_ID, this.accountId);
            map.putString(CALLBACK_KEY_VIDEO_ID, session.videoId);
            map.putString(CALLBACK_KEY_VIDEO_TOKEN, session.videoId);
            map.putInt(CALLBACK_KEY_VIDEO_SIZE_IN_MB, (int) session.downloadSizeInMb);
            map.putDouble(CALLBACK_KEY_DOWNLOAD_PROGRESS, session.downloadProgress);
            statuses.pushMap(map);
        }

        return statuses;
    }

    private NativeArray collectNativePlaylist(String accountId, Playlist playlist) {
        WritableNativeArray result = new WritableNativeArray();
        List<Video> videos = playlist.getVideos();
        for (int i = 0; i < videos.size(); i++) {
            Video video = videos.get(i);
            WritableNativeMap map = new WritableNativeMap();
            map.putString(CALLBACK_KEY_ACCOUNT_ID, accountId);
            map.putString(CALLBACK_KEY_VIDEO_ID, video.getId());
            map.putString(CALLBACK_KEY_REFERENCE_ID, video.getReferenceId());
            map.putString(CALLBACK_KEY_NAME, video.getName());
            map.putString(CALLBACK_KEY_DESCRIPTION, video.getDescription());
            map.putInt(CALLBACK_KEY_DURATION, video.getDuration());
            result.pushMap(map);
        }
        return result;
    }

    private boolean hasOfflineVideoDownloadSessionWithReferenceId(String referenceId) {
        for (OfflineVideoDownloadSession session : this.offlineVideoDownloadSessions) {
            if (referenceId.equals(session.referenceId)) return true;
        }
        return false;
    }

    private boolean hasOfflineVideoDownloadSessionWithVideoId(String videoId) {
        for (OfflineVideoDownloadSession session : this.offlineVideoDownloadSessions) {
            if (videoId.equals(session.videoId)) return true;
        }
        return false;
    }

    @Override
    public void onCompleted(OfflineVideoDownloadSession session) {
        this.offlineVideoDownloadSessions.remove(session);
        if (session.downloadProgress == 1) {
            Map<String, Object> param = new HashMap<>();
            param.put(Video.Fields.ID, session.videoId);
            Video video = new Video(param);
            this.allDownloadedVideos.put(video.getId(), video);
            this.offlineCatalog.getVideoDownloadStatus(video);
        }
        this.listener.onOfflineStorageStateChanged(collectNativeOfflineVideoStatuses());
    }

    @Override
    public void onProgress() {
        this.listener.onOfflineStorageStateChanged(collectNativeOfflineVideoStatuses());
    }
}
