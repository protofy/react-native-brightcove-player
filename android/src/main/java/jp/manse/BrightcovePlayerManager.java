package jp.manse;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.facebook.infer.annotation.Assertions;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.common.MapBuilder;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;

import java.util.HashMap;
import java.util.Map;


public class BrightcovePlayerManager extends SimpleViewManager<BrightcovePlayerView> {
    public static final String REACT_CLASS = "BrightcovePlayer";
    public static final int COMMAND_SEEK_TO = 1;
    public static final int COMMAND_STOP_PLAYBACK = 2;
    public static final int COMMAND_SHOW_CAPTIONS = 3;
    public static final String EVENT_READY = "ready";
    public static final String EVENT_PLAY = "play";
    public static final String EVENT_PAUSE = "pause";
    public static final String EVENT_END = "end";
    public static final String EVENT_PROGRESS = "progress";
    public static final String EVENT_TOGGLE_ANDROID_FULLSCREEN = "toggle_android_fullscreen";
    public static final String EVENT_CHANGE_DURATION = "change_duration";
    public static final String EVENT_UPDATE_BUFFER_PROGRESS = "update_buffer_progress";
    public static final String EVENT_CLOSED_CAPTIONS_LOADED = "closed_captions_loaded";

    private final ReactApplicationContext applicationContext;

    public BrightcovePlayerManager(ReactApplicationContext context) {
        super();
        this.applicationContext = context;
    }

    @NonNull
    @Override
    public String getName() {
        return REACT_CLASS;
    }

    @NonNull
    @Override
    public BrightcovePlayerView createViewInstance(@NonNull ThemedReactContext ctx) {
        return new BrightcovePlayerView(ctx, applicationContext);
    }

    @ReactProp(name = "policyKey")
    public void setPolicyKey(BrightcovePlayerView view, String policyKey) {
        view.setPolicyKey(policyKey);
    }

    @ReactProp(name = "accountId")
    public void setAccountId(BrightcovePlayerView view, String accountId) {
        view.setAccountId(accountId);
    }

    @ReactProp(name = "videoId")
    public void setVideoId(BrightcovePlayerView view, String videoId) {
        view.setVideoId(videoId);
    }

    @ReactProp(name = "referenceId")
    public void setReferenceId(BrightcovePlayerView view, String referenceId) {
        view.setReferenceId(referenceId);
    }

    @ReactProp(name = "videoToken")
    public void setVideoToken(BrightcovePlayerView view, String videoToken) {
        view.setVideoToken(videoToken);
    }

    @ReactProp(name = "autoPlay")
    public void setAutoPlay(BrightcovePlayerView view, boolean autoPlay) {
        view.setAutoPlay(autoPlay);
    }

    @ReactProp(name = "allowExternalPlayback")
    public void setAllowExternalPlayback(BrightcovePlayerView view, boolean allowExternalPlayback) {
        view.setAllowExternalPlayback(allowExternalPlayback);
    }

    @ReactProp(name = "play")
    public void setPlay(BrightcovePlayerView view, boolean play) {
        view.setPlay(play);
    }

    @ReactProp(name = "disableDefaultControl")
    public void setDefaultControlDisabled(BrightcovePlayerView view, boolean disableDefaultControl) {
        view.setDefaultControlDisabled(disableDefaultControl);
    }

    @ReactProp(name = "volume")
    public void setVolume(BrightcovePlayerView view, float volume) {
        view.setVolume(volume);
    }

    @ReactProp(name = "bitRate")
    public void setBitRate(BrightcovePlayerView view, float bitRate) {
        view.setBitRate((int) bitRate);
    }

    @ReactProp(name = "playbackRate")
    public void setPlaybackRate(BrightcovePlayerView view, float playbackRate) {
        view.setPlaybackRate(playbackRate);
    }

    @ReactProp(name = "fullscreen")
    public void setFullscreen(BrightcovePlayerView view, boolean fullscreen) {
        view.setFullscreen(fullscreen);
    }

    @Override
    public Map<String, Integer> getCommandsMap() {
        return MapBuilder.of(
                "seekTo",
                COMMAND_SEEK_TO,
                "stopPlayback",
                COMMAND_STOP_PLAYBACK,
                "showCaptions",
                COMMAND_SHOW_CAPTIONS
        );
    }

    @Override
    public void receiveCommand(@NonNull BrightcovePlayerView view, int commandId, ReadableArray args) {
        Assertions.assertNotNull(view);
        Assertions.assertNotNull(args);
        switch (commandId) {
            case COMMAND_SEEK_TO: {
                if (args != null) {
                    view.seekTo((int) (args.getDouble(0) * 1000));
                }
                return;
            }
            case COMMAND_STOP_PLAYBACK: {
                view.stopPlayback();
            }
            case COMMAND_SHOW_CAPTIONS: {
                view.showCaptions();
            }
        }
    }

    @Override
    public @Nullable
    Map<String, Object> getExportedCustomDirectEventTypeConstants() {
        Map<String, Object> map = new HashMap<>();
        map.put(EVENT_READY, MapBuilder.of("registrationName", "onReady"));
        map.put(EVENT_PLAY, MapBuilder.of("registrationName", "onPlay"));
        map.put(EVENT_PAUSE, MapBuilder.of("registrationName", "onPause"));
        map.put(EVENT_END, MapBuilder.of("registrationName", "onEnd"));
        map.put(EVENT_PROGRESS, MapBuilder.of("registrationName", "onProgress"));
        map.put(EVENT_CHANGE_DURATION, MapBuilder.of("registrationName", "onChangeDuration"));
        map.put(EVENT_UPDATE_BUFFER_PROGRESS, MapBuilder.of("registrationName", "onUpdateBufferProgress"));
        map.put(EVENT_TOGGLE_ANDROID_FULLSCREEN, MapBuilder.of("registrationName", "onToggleAndroidFullscreen"));
        map.put(EVENT_CLOSED_CAPTIONS_LOADED, MapBuilder.of("registrationName", "onClosedCaptionsLoaded"));
        return map;
    }
}
