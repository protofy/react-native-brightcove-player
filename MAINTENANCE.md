# Maintenance
This library is maintained by Protofy. Here is how:

## Brightcove Version

### iOS
The sdk version is pinned within `react-native-brightcove-player.podspec`, e.g. `s.dependency          'Brightcove-Player-Core', '6.12.1'`.

You'll find the sdk release notes [here](https://github.com/brightcove/brightcove-player-sdk-ios/blob/master/CHANGELOG.md).

## Android
The sdk version is pinned within `android/build.gradle`: e.g. `implementation('com.brightcove.player:exoplayer2:8.2.1')`.

You'll find the sdk release notes [here](https://sdks.support.brightcove.com/android/resources/brightcove-native-sdk-android-release-notes.html).

## Upgrading React Native in Sample Project
When upgrading react native after a long time a lot changes have to be made. Depending on the number and severity of version upgrades it might be a good idea to create a new sample project with a react native setup. Then try to migrate the code of the current sample app to the new one.

## Troubleshooting

### iOS

Videos only show black screen while playing sound? iOS 16.4 Simulators have issues with playing drm protected content, see [this issue](https://developer.apple.com/forums/thread/727288). Instead you should use a different iOS Simulator (below 16.4 or 17+) or a real device.