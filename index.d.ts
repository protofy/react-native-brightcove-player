import * as React from 'react';
import {ViewStyle} from 'react-native';

type VideoToken = string;

export interface Caption {
  uri: string;
  label: string;
  language: string;
  type: string;
  default: boolean;
  hasInBandMetadataTrackDispatchType: boolean;
}

export interface CaptionsLoadedEventParams {
  captions: Caption[];
}

export type PersistenceWrite = (key: string, value: string) => Promise<unknown>;

export type PersistenceDelete = (key: string) => Promise<unknown>;

export type PersistenceRead = (key: string) => Promise<string | null>;

interface CaptionPersistenceService {
  getPreferredSubtitleLanguages: () => Promise<string | null>,
  addPreferredSubtitleLanguage: (language: string) => Promise<void>,
  getActiveCaption: (captions: Caption[]) => Promise<Caption | undefined>,
  resetCaptionSelection: () => Promise<void>;
}

export function getCaptionService(read: PersistenceRead, write: PersistenceWrite, remove: PersistenceDelete): CaptionPersistenceService;

export type BrightcovePlayerProps = {
  policyKey?: string;
  accountId?: string;
  referenceId?: string;
  videoId?: string;
  videoToken?: VideoToken;
  autoPlay?: boolean;
  allowExternalPlayback?: boolean;
  play?: boolean;
  fullscreen?: boolean;
  disableDefaultControl?: boolean;
  volume?: number;
  bitRate?: number;
  playbackRate?: number;
  onReady?: () => void;
  onPlay?: () => void;
  onPause?: () => void;
  onEnd?: () => void;
  onProgress?: ({currentTime}: {currentTime: number}) => void;
  onChangeDuration?: ({duration}: {duration: number}) => void;
  onUpdateBufferProgress?: ({
                              bufferProgress,
                            }: {
    bufferProgress: number;
  }) => void;
  onEnterFullscreen?: () => void;
  onExitFullscreen?: () => void;
  onClosedCaptionsLoaded?: (loadedEvent: CaptionsLoadedEventParams) => void;
  style?: ViewStyle;
};

export class BrightcovePlayer extends React.Component<BrightcovePlayerProps,
  {}> {
  seekTo(position: number): {};

  showCaptions: () => void;
}

export type BrightcovePlayerPosterProps = {
  policyKey?: string;
  accountId?: string;
  referenceId?: string;
  videoId?: string;
  videoToken?: VideoToken;
  style?: ViewStyle;
};

export class BrightcovePlayerPoster extends React.Component<BrightcovePlayerPosterProps,
  {}> {
}

export namespace BrightcovePlayerUtil {
  type PlaylistVideo = {
    accountId: string;
    videoId: string;
    referenceId: string;
    name: string;
    description: string;
    duration: number;
  };

  type OfflineVideoStatus = {
    accountId: string;
    videoId: string;
    downloadProgress: number;
    videoToken: VideoToken;
    videoSizeInMegabyte: number;
  };

  export function requestDownloadVideoWithReferenceId(
    accountId: string,
    policyKey: string,
    referenceId: string,
    bitRate?: number,
  ): Promise<VideoToken>;

  export function requestDownloadVideoWithVideoId(
    accountId: string,
    policyKey: string,
    videoId: string,
    bitRate?: number,
  ): Promise<VideoToken>;

  export function getOfflineVideoStatuses(
    accountId: string,
    policyKey: string,
  ): Promise<OfflineVideoStatus[]>;

  export function deleteOfflineVideo(
    accountId: string,
    policyKey: string,
    videoToken: VideoToken,
  ): Promise<void>;

  export function getPlaylistWithReferenceId(
    accountId: string,
    policyKey: string,
    referenceId: string,
  ): Promise<PlaylistVideo[]>;

  export function getPlaylistWithPlaylistId(
    accountId: string,
    policyKey: string,
    playlistId: string,
  ): Promise<PlaylistVideo[]>;

  export function addOfflineNotificationListener(
    callback: (statuses: OfflineVideoStatus[]) => void,
  ): {remove: Function};

  export function getEstimatedSizeOfDownloadedVideo(
    videoToken: string,
    accountId: string,
    policyKey: string,
  ): Promise<number>;
}
