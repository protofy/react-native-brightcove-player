const BrightcovePlayer = require('./src/BrightcovePlayer');
const BrightcovePlayerPoster = require('./src/BrightcovePlayerPoster');
const BrightcovePlayerUtil = require('./src/BrightcovePlayerUtil');
const {getCaptionService} = require('./src/CaptionUtils');

module.exports = {
  BrightcovePlayer: BrightcovePlayer,
  BrightcovePlayerPoster: BrightcovePlayerPoster,
  BrightcovePlayerUtil: BrightcovePlayerUtil,
  getCaptionService: getCaptionService,
};
