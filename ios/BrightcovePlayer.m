#import "BrightcovePlayer.h"
#import "BrightcovePlayerOfflineVideoManager.h"
#import "BrightcovePlayerUtil.h"

@interface BrightcovePlayer () <BCOVPlaybackControllerDelegate, BCOVPUIPlayerViewDelegate>
@property NSString* loadedVideoToken;
@property NSMutableArray<id> *currentPlayTargets;
@property NSMutableArray<id> *currentPauseTargets;
@end

@implementation BrightcovePlayer

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setup];
        [self setupOfflineVideoTokenObserver];
    }
    return self;
}

- (void)dealloc
{
    [NSNotificationCenter.defaultCenter removeObserver:self];
}

- (void)setupOfflineVideoTokenObserver {

    [NSNotificationCenter.defaultCenter addObserver:self selector:@selector(didRemoveOfflineVideoToken:) name:BrightcovePlayerUtil.didRemoveOfflineVideoTokenNotificationName object:nil];
}

- (void)didRemoveOfflineVideoToken:(NSNotification*)notification {
    NSDictionary *dict = notification.userInfo;
    NSString* token = [dict valueForKey:BrightcovePlayerUtil.kDidRemoveOfflineVideoToken];

    if ([self.loadedVideoToken isEqualToString:token]) {
        NSLog(@"%@ %s TOKEN DELETED: %@", self, __FUNCTION__, token);
        self.loadedVideoToken = nil;
    }
}

- (void)setup {

    self.currentPlayTargets = [[NSMutableArray alloc] init];
    self.currentPauseTargets = [[NSMutableArray alloc] init];

    self.playbackController = [BCOVPlayerSDKManager.sharedManager createPlaybackController];
    self.playbackController.delegate = self;
    self.playbackController.autoPlay = NO;
    self.playbackController.autoAdvance = YES;
    self.playbackController.allowsBackgroundAudioPlayback = YES;


    self.playerView = [[BCOVPUIPlayerView alloc] initWithPlaybackController:self.playbackController options:nil controlsView:[BCOVPUIBasicControlView basicControlViewWithVODLayout] ];
    self.playerView.delegate = self;
    self.playerView.backgroundColor = UIColor.blackColor;

    self.targetVolume = 1.0;
    self.autoPlay = NO;

    [self addSubview:self.playerView];
    self.playerView.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray* constraints = [NSArray arrayWithObjects:[self.playerView.topAnchor constraintEqualToAnchor:self.topAnchor],
                            [self.playerView.leftAnchor constraintEqualToAnchor:self.leftAnchor],
                            [self.playerView.rightAnchor constraintEqualToAnchor:self.rightAnchor],
                            [self.playerView.bottomAnchor constraintEqualToAnchor:self.bottomAnchor],
                            nil];
    [NSLayoutConstraint activateConstraints:constraints];
}

- (void)setupService {
    if ((!_playbackService || _playbackServiceDirty) && _accountId && _policyKey) {
        _playbackServiceDirty = NO;
        _playbackService = [[BCOVPlaybackService alloc] initWithAccountId:_accountId policyKey:_policyKey];
    }
}

- (void)loadMovie {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        if (self.videoToken) {
            BCOVVideo *video = [[BrightcovePlayerOfflineVideoManager sharedManager] videoObjectFromOfflineVideoToken:self.videoToken];
            if (video && ![self.loadedVideoToken isEqualToString:self.videoToken]) {
                [self.playbackController setVideos: @[ video ]];
                self.loadedVideoToken = video.properties[kBCOVOfflineVideoTokenPropertyKey];
                NSLog(@"%@ %s SET VIDEO %@ FOR TOKEN: %@", self, __FUNCTION__, video.properties[kBCOVVideoPropertyKeyName], self.videoToken);
            }
            return;
        }
        if (!self.playbackService) return;
        if (self.videoId) {
            [self.playbackService findVideoWithVideoID:self.videoId parameters:nil completion:^(BCOVVideo *video, NSDictionary *jsonResponse, NSError *error) {
                if (video) {
                    [self.playbackController setVideos: @[ video ]];
                    [self closedCaptionsLoaded:video];
                }
            }];
        } else if (self.referenceId) {
            [self.playbackService findVideoWithReferenceID:self.referenceId parameters:nil completion:^(BCOVVideo *video, NSDictionary *jsonResponse, NSError *error) {
                if (video) {
                    [self.playbackController setVideos: @[ video ]];
                    [self closedCaptionsLoaded:video];
                }
            }];
        }
    });
}

- (id<BCOVPlaybackController>)createPlaybackController {
    BCOVBasicSessionProviderOptions *options = [BCOVBasicSessionProviderOptions alloc];
    BCOVBasicSessionProvider *provider = [[BCOVPlayerSDKManager sharedManager] createBasicSessionProviderWithOptions:options];
    return [BCOVPlayerSDKManager.sharedManager createPlaybackControllerWithSessionProvider:provider viewStrategy:nil];
}

- (void)setReferenceId:(NSString *)referenceId {
    _referenceId = referenceId;
    _videoId = NULL;
    [self setupService];
    [self loadMovie];
}

- (void)setVideoId:(NSString *)videoId {
    _videoId = videoId;
    _referenceId = NULL;
    [self setupService];
    [self loadMovie];
}

- (void)setVideoToken:(NSString *)videoToken {
    _videoToken = videoToken;
    [self loadMovie];
}

- (void)setAccountId:(NSString *)accountId {
    _accountId = accountId;
    _playbackServiceDirty = YES;
    [self setupService];
    [self loadMovie];
}

- (void)setPolicyKey:(NSString *)policyKey {
    _policyKey = policyKey;
    _playbackServiceDirty = YES;
    [self setupService];
    [self loadMovie];
}

- (void)setAutoPlay:(BOOL)autoPlay {
    _autoPlay = autoPlay;
}

- (void) enableAudioSession {
    AVAudioSession *session = [AVAudioSession sharedInstance];
    NSError *error = nil;

    [session
     setCategory:AVAudioSessionCategoryPlayback
     mode:AVAudioSessionModeMoviePlayback
     options:AVAudioSessionCategoryOptionDuckOthers|AVAudioSessionCategoryOptionAllowAirPlay|AVAudioSessionCategoryOptionAllowBluetooth
     error:&error
    ];

    if (error != nil) {
        NSLog(@"[AudioSession]: Error while setting `AVAudioSessionCategory`: %@ %@", error, [error userInfo]);
        return;
    }

    [session setActive:true error:&error];

    if (error != nil) {
        NSLog(@"[AudioSession]: Cannot start audio session: %@ %@", error, [error userInfo]);
    }

    [UIApplication.sharedApplication beginReceivingRemoteControlEvents];
}

- (void) disableAudioSession {
    AVAudioSession *session = [AVAudioSession sharedInstance];
    NSError *error = nil;

    [self setPlay:false];
    [session setCategory:AVAudioSessionCategorySoloAmbient error:&error];

    if (error != nil) {
        NSLog(@"[AudioSession]: Cannot set `AVAudioSessionCategory`: %@ %@", error, [error userInfo]);

    }

    [session setActive:false error:&error];
    [self cleanupNowPlayingInfo];

    if (error != nil) {
        NSLog(@"[AudioSession]: Cannot stop audio session: %@ %@", error, [error userInfo]);
    }

    [UIApplication.sharedApplication endReceivingRemoteControlEvents];
}

/**
 * Controls whether the AirPlay button will be visible.
 * @param allowExternalPlayback Whether AirPlay can be targeted
 */
- (void)setAllowExternalPlayback:(BOOL)allowExternalPlayback {
    NSLog(@"[airplay]: External playback will be %@.", allowExternalPlayback ? @"activated" : @"deactivated");

    [_playbackController setAllowsExternalPlayback:allowExternalPlayback];

    if (allowExternalPlayback) {
        [self enableAudioSession];
    } else {
        [self disableAudioSession];
    }
}

- (void)setPlay:(BOOL)play {
    if (self.playing == play) return;
    if (play) {
        [self.playbackController play];
    } else {
        [self.playbackController pause];
    }
}

- (void)setFullscreen:(BOOL)fullscreen {
    if (fullscreen) {
        [_playerView performScreenTransitionWithScreenMode:BCOVPUIScreenModeFull];
    } else {
        [_playerView performScreenTransitionWithScreenMode:BCOVPUIScreenModeNormal];
    }
}

- (void)setVolume:(NSNumber*)volume {
    _targetVolume = volume.doubleValue;
    [self refreshVolume];
}

- (void)setBitRate:(NSNumber*)bitRate {
    _targetBitRate = bitRate.doubleValue;
    [self refreshBitRate];
}

- (void)setPlaybackRate:(NSNumber*)playbackRate {
    _targetPlaybackRate = playbackRate.doubleValue;
    if (_playing) {
        [self refreshPlaybackRate];
    }
}

- (void)refreshVolume {
    if (!_playbackSession) return;
    _playbackSession.player.volume = _targetVolume;
}

- (void)refreshBitRate {
    if (!_playbackSession) return;
    AVPlayerItem *item = _playbackSession.player.currentItem;
    if (!item) return;
    item.preferredPeakBitRate = _targetBitRate;
}

- (void)refreshPlaybackRate {
    if (!_playbackSession || !_targetPlaybackRate) return;
    _playbackSession.player.rate = _targetPlaybackRate;
}

- (void)setDisableDefaultControl:(BOOL)disable {
    _playerView.controlsView.hidden = disable;
}

- (void)seekTo:(NSNumber *)time {
    [_playbackController seekToTime:CMTimeMakeWithSeconds([time floatValue], NSEC_PER_SEC) completionHandler:^(BOOL finished) {
    }];
}

- (void)playbackController:(id<BCOVPlaybackController>)controller playbackSession:(id<BCOVPlaybackSession>)session didReceiveLifecycleEvent:(BCOVPlaybackSessionLifecycleEvent *)lifecycleEvent {
    if (lifecycleEvent.eventType == kBCOVPlaybackSessionLifecycleEventPlaybackBufferEmpty || lifecycleEvent.eventType == kBCOVPlaybackSessionLifecycleEventFail ||
        lifecycleEvent.eventType == kBCOVPlaybackSessionLifecycleEventError ||
        lifecycleEvent.eventType == kBCOVPlaybackSessionLifecycleEventTerminate) {
        _playbackSession = nil;
        return;
    }
    _playbackSession = session;
    if (lifecycleEvent.eventType == kBCOVPlaybackSessionLifecycleEventReady) {
        [self refreshVolume];
        [self refreshBitRate];
        if (self.onReady) {
            self.onReady(@{});
        }
        if (_autoPlay) {
            [_playbackController play];
        }
    } else if (lifecycleEvent.eventType == kBCOVPlaybackSessionLifecycleEventPlay) {
        _playing = true;
        [self refreshPlaybackRate];
        if (self.onPlay) {
            [self updateNowPlayingInfo];
            self.onPlay(@{});
        }
    } else if (lifecycleEvent.eventType == kBCOVPlaybackSessionLifecycleEventPause) {
        _playing = false;
        if (self.onPause) {
            [self updateNowPlayingInfo];
            self.onPause(@{});
        }
    } else if (lifecycleEvent.eventType == kBCOVPlaybackSessionLifecycleEventEnd) {
        if (self.onEnd) {
            self.onEnd(@{});
        }
    }
}

- (void)playbackController:(id<BCOVPlaybackController>)controller playbackSession:(id<BCOVPlaybackSession>)session didChangeDuration:(NSTimeInterval)duration {
    if (self.onChangeDuration) {
        self.onChangeDuration(@{
                                @"duration": @(duration)
                                });
    }
}

-(void)playbackController:(id<BCOVPlaybackController>)controller playbackSession:(id<BCOVPlaybackSession>)session didProgressTo:(NSTimeInterval)progress {
    if (self.onProgress && progress > 0 && progress != INFINITY) {
        self.onProgress(@{
                          @"currentTime": @(progress)
                          });
    }
    float bufferProgress = _playerView.controlsView.progressSlider.bufferProgress;
    if (_lastBufferProgress != bufferProgress) {
        _lastBufferProgress = bufferProgress;
        self.onUpdateBufferProgress(@{
                                      @"bufferProgress": @(bufferProgress),
                                      });
    }
}

- (void)playbackController:(id<BCOVPlaybackController>)controller playbackSession:(id<BCOVPlaybackSession>)session determinedMediaType:(BCOVSourceMediaType)mediaType
{
    NSLog(@"[mediatype] Media Type: %ld", (long)mediaType);
    switch (mediaType)
    {
        case BCOVSourceMediaTypeAudio:
            NSLog(@"Source is Audio Only");
            [self updateNowPlayingInfo];
            break;
        default:
            [self updateNowPlayingInfo];
            break;
    }
}

-(void)playerView:(BCOVPUIPlayerView *)playerView didTransitionToScreenMode:(BCOVPUIScreenMode)screenMode {
    if (screenMode == BCOVPUIScreenModeNormal) {
        if (self.onExitFullscreen) {
            self.onExitFullscreen(@{});
        }
    } else if (screenMode == BCOVPUIScreenModeFull) {
        if (self.onEnterFullscreen) {
            self.onEnterFullscreen(@{});
        }
    }
}

-(void)cleanupNowPlayingInfo {
    MPNowPlayingInfoCenter *infoCenter = [MPNowPlayingInfoCenter defaultCenter];
    MPRemoteCommandCenter *commandCenter = [MPRemoteCommandCenter sharedCommandCenter];

    for (id playTarget in self.currentPlayTargets) {
        [[[MPRemoteCommandCenter sharedCommandCenter] playCommand] removeTarget:playTarget];
    }

    for (id playTarget in self.currentPlayTargets) {
        [[[MPRemoteCommandCenter sharedCommandCenter] pauseCommand] removeTarget:playTarget];
    }

    [[self currentPlayTargets] removeAllObjects];
    [[self currentPauseTargets] removeAllObjects];

    infoCenter.nowPlayingInfo = nil;
}

-(void)closedCaptionsLoaded:(BCOVVideo*)video {
    NSArray *tracks = video.properties[kBCOVVideoPropertyKeyTextTracks];

    NSMutableArray *captions = [[NSMutableArray alloc] init];
    for (NSDictionary* track in tracks) {
        NSString *kind = [track objectForKey:@"kind"];
        if ([kind isEqual:@"captions"] || [kind isEqual:@"subtitles"]) {
            [captions addObject:@{
                @"language": track[@"srclang"],
                @"label": track[@"label"],
                @"default": track[@"default"],
                @"hasInBandMetadataTrackDispatchType": track[@"in_band_metadata_track_dispatch_type"],
                @"uri": track[@"src"],
                @"type": track[@"mime_type"]
            }];
        }
    }


    self.onClosedCaptionsLoaded(@{
        @"captions": captions
    });
}

-(void)updateNowPlayingInfo {
    MPNowPlayingInfoCenter *infoCenter = [MPNowPlayingInfoCenter defaultCenter];
    MPRemoteCommandCenter *commandCenter = [MPRemoteCommandCenter sharedCommandCenter];
    NSMutableDictionary *nowPlayingInfo = @{}.mutableCopy;

    [[commandCenter previousTrackCommand] setEnabled:false];
    [[commandCenter nextTrackCommand] setEnabled:false];

    nowPlayingInfo[MPMediaItemPropertyTitle] = localizedNameForLocale(_playbackSession.video, nil);
    NSNumber *duration = _playbackSession.video.properties[kBCOVVideoPropertyKeyDuration];
    nowPlayingInfo[MPMediaItemPropertyPlaybackDuration] = @(duration.doubleValue / 1000);
    nowPlayingInfo[MPMediaItemPropertyMediaType] = @(MPMediaTypePodcast);

    NSString *posterURLString = _playbackSession.video.properties[kBCOVVideoPropertyKeyPoster];
    NSURL *posterURL = [NSURL URLWithString:posterURLString];

    id playTarget = [commandCenter.playCommand addTargetWithHandler:^MPRemoteCommandHandlerStatus(MPRemoteCommandEvent * _Nonnull event) {
        NSLog(@"[airplay]: Play Command Executed");
        [self setPlay:true];
        return MPRemoteCommandHandlerStatusSuccess;
    }];
    [self.currentPlayTargets addObject:playTarget];
    id pauseTarget = [commandCenter.pauseCommand addTargetWithHandler:^MPRemoteCommandHandlerStatus(MPRemoteCommandEvent * _Nonnull event) {
        NSLog(@"[airplay]: Pause Command Executed");
        [self setPlay:false];
        return MPRemoteCommandHandlerStatusSuccess;
    }];
    [self.currentPauseTargets addObject:pauseTarget];

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:posterURL];
        UIImage *image = [UIImage imageWithData:imageData];
        nowPlayingInfo[MPMediaItemPropertyArtwork] = [[MPMediaItemArtwork alloc] initWithBoundsSize:image.size requestHandler:^UIImage * _Nonnull(CGSize size) {
            return image;
        }];
        infoCenter.nowPlayingInfo = nowPlayingInfo;
    });

    infoCenter.nowPlayingInfo = nowPlayingInfo;
}

-(void)dispose {
    [self disableAudioSession];
    [self.playbackController setVideos:@[]];
}

@end
